<?php

namespace ActiveAnts\TranslatableTitles\Plugin\View\Page\Config;

use Magento\Framework\View\Page\Config\Renderer;

class RendererPlugin
{
    /**
     * Intercepts the renderTitle function from the @var $subject \Magento\Framework\View\Page\Config\Renderer,
     * strips and trims any HTML tags and returns it enclosed with HTML title tags, followed by a newline tag.
     *
     * @param Renderer $subject
     * @param $title
     * @return string
     */
    public function afterRenderTitle(Renderer $subject, $title)
    {
        $translatableTitle = trim(strip_tags($title));

        return '<title>' . __($translatableTitle) . '</title>' . "\n";
    }
}