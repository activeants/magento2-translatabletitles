# [![Active Ants](http://activeants.nl/content/themes/active-ants/img/logo.png)](http://activeants.nl/) 

## Magento 2 translatable titles
This module adds a plugin to intercept the Magento 2 title renderer, the plugin takes the title string and returns it using the PHP translation function which allows .csv translations to be used for page title translations.

##Motivation
We have built a small collection of Dutch Magento 2 shops that use a Dutch translation pack and it really bugged us that some page titles did not translate correctly.

## Installation
```
composer require activeants/magento2-translatabletitles
php bin/magento module:enable ActiveAnts_TranslatableTitles
php bin/magento setup:upgrade
```

## Issues
Feel free to report any issues for this project using the integrated [Bitbucket issue tracker](https://bitbucket.org/activeants/magento2-translatabletitles/issues), we will try to get back to issues as fast as possible.

## License
The translatable titles module is free software, and is released under the terms of the OSL 3.0 license. See LICENSE.md for more information.

## Credits
[Active Ants](http://activeants.nl/) provides e-commerce efulfilment services for webshops. We store products, pick, pack and ship them. But we do much more. With unique efulfilment solutions we make our clients, the webshops, more successful.

~ The [Active Ants](http://activeants.nl/) software development team.